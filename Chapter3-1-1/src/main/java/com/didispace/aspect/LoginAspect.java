package com.didispace.aspect;

import com.didispace.model.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/26 14:09
 * @company 蜂网供应链管理（上海）有限公司
 */
@Aspect
@Order(1)
@Component
public class LoginAspect {

    public LoginAspect() {
    }

    @Pointcut("execution(* com.didispace.controller.*.*(..))")
    public void cut() {
    }

    @Before("cut()")
    public void beforeNotice(JoinPoint point) {
        System.out.println("beforeNotice");
        Object[] parameterValues = point.getArgs();
        Class<?>[] paramenterTypes = ((MethodSignature)point.getSignature()).getMethod().getParameterTypes();
        String[] parameterNames = ((CodeSignature)point.getSignature()).getParameterNames();
        Class clazz = point.getTarget().getClass();

        try {
           Method m = clazz.getMethod(point.getSignature().getName(), paramenterTypes);
//            Logger dto = this.create(this.level, this.env, paramenterTypes, parameterNames, parameterValues, clazz, m, this.systemName, this.systemVersion);
//            loggerList.putIfAbsent(this.threadKey(clazz, m), dto);
        } catch (Exception e){

        }
    }

    @AfterReturning(
            pointcut = "cut()",
            returning = "obj",
            argNames = "obj"
    )
    public void afterNotice(JoinPoint args, Object obj) {
        System.out.println("afterNotice");
    }

    @AfterThrowing(
            pointcut = "cut()",
            throwing = "e"
    )
    public void exceptionNotice(JoinPoint args, Exception e) {
        System.out.println("exceptionNotice");
    }
}
