package com.didispace.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/26 14:13
 * @company 蜂网供应链管理（上海）有限公司
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/show")
    public String show() {
        return "OK";
    }

    @RequestMapping("/show2")
    public String show2() {
        return "OK2";
    }
}
