package com.didispace.enums;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/26 14:27
 * @company 蜂网供应链管理（上海）有限公司
 */
public enum RequestStatusEnum {
    SUCCESS,
    FAILURE;

    private RequestStatusEnum() {
    }
}
