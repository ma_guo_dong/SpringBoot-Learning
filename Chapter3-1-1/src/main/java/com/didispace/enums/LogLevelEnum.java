package com.didispace.enums;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/26 14:27
 * @company 蜂网供应链管理（上海）有限公司
 */
public enum  LogLevelEnum {

    INFO,
    ERROR;

    private LogLevelEnum() {
    }

    public static LogLevelEnum getLevel(String level) {
        if (null != level) {
            if (level.equals(INFO.name())) {
                return INFO;
            } else {
                return level.equals(ERROR.name()) ? ERROR : null;
            }
        } else {
            return null;
        }
    }
}
