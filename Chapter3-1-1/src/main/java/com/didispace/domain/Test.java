package com.didispace.domain;

import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/23 10:24
 * @company 蜂网供应链管理（上海）有限公司
 */
public class Test {
    static String  base = "string";


    public static void main(String[] args) {

        ExecutorService executorService= Executors.newFixedThreadPool(5);

        for (int i = 0; i < 20; i++) {
            final int index = i;
            executorService.execute(new Runnable() {

                @Override
                public void run() {
                    try {
                        System.out.println(index+Thread.currentThread().getName());
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        System.out.println("完成");

    }
}
