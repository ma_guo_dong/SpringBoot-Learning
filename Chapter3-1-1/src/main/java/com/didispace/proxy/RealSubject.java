package com.didispace.proxy;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/30 17:18
 * @company 蜂网供应链管理（上海）有限公司
 */
public class RealSubject implements Subject {

    @Override
    public void doSomething() {
        System.out.println( "call doSomething()" );
    }
}
