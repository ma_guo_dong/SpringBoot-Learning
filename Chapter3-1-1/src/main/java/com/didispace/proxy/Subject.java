package com.didispace.proxy;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/30 17:18
 * @company 蜂网供应链管理（上海）有限公司
 */
public interface Subject {
    public void doSomething();
}
