package com.didispace.proxy;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.Proxy;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/7/30 17:20
 * @company 蜂网供应链管理（上海）有限公司
 */
public class DynamicProxy {

    public static void main(String args[]) {
        RealSubject real = new RealSubject();
        Subject proxySubject = (Subject) Proxy.newProxyInstance(Subject.class.getClassLoader(),
                new Class[]{Subject.class},
                new ProxyHandler(real));

        proxySubject.doSomething();

        //write proxySubject class binary data to file
        createProxyClassFile();
    }
    public static void createProxyClassFile() {
        String name = "ProxySubject";
        byte[] data = ProxyGenerator.generateProxyClass( name, new Class[] { Subject.class } );
        try {
            FileOutputStream out = new FileOutputStream( name + ".class" );
            out.write(data);
            out.close();
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }
}
