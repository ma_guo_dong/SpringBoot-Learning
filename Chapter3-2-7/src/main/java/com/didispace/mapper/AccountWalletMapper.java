package com.didispace.mapper;

import com.didispace.entity.AccountWallet;

/**
 * Created by 丁文豪 on 2018/3/7.
 */
public interface AccountWalletMapper {

    AccountWallet selectByOpenId(String openId);

    int updateAccountWallet(AccountWallet record);
}
