package com.didispace.test;

import com.didispace.entity.AccountWallet;
import com.didispace.entity.Student;
import com.didispace.util.CloneUtil;
import com.didispace.util.SerializationUtils;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/4/20 13:00
 * @company 蜂网供应链管理（上海）有限公司
 */
public class TestSerila {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        AccountWallet accountWallet=new AccountWallet();
        List<Student> students=new ArrayList<>();
        students.add(new Student("小明4"));
        students.add(new Student("小明1"));
        students.add(new Student("小明2"));
        students.add(new Student("小明3"));
        accountWallet.setAmount(new BigDecimal(10));
        accountWallet.setStudentList(students);
        AccountWallet accountWallet12= (AccountWallet) CloneUtil.deepClone(accountWallet);
        accountWallet.setAmount(new BigDecimal(2));
        accountWallet.getStudentList().get(0).setName("aa");
        System.out.println(accountWallet12.getStudentList().get(0).getName());
        accountWallet12.setAmount(new BigDecimal(20));
        System.out.println(accountWallet.getAmount());
    }
}
