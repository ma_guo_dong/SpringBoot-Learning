package com.didispace.util;

/**
 * @author iw
 * @create 2017-12-20 12:51
 **/
public class HttpResponse<T> {

    private boolean status;
    private T data;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
