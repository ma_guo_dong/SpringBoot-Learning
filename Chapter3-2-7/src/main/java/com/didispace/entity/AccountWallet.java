package com.didispace.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 丁文豪 on 2018/3/7.
 */
public class AccountWallet implements Serializable{

    private Integer id;
    private String openId;
    private BigDecimal amount;
    private Date createTime;
    private Date updateTime;
    private String password;
    private Integer isOpen;
    private String checkKey;
    private Integer version;
    private List<Student> studentList;

    public Integer getId() {
        return id;
    }

    public String getOpenId() {
        return openId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public String getPassword() {
        return password;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public String getCheckKey() {
        return checkKey;
    }

    public Integer getVersion() {
        return version;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Student> getStudentList() {
        return studentList;
    }
}
