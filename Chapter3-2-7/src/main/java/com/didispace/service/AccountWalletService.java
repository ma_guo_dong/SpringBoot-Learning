package com.didispace.service;

import com.didispace.entity.AccountWallet;

/**
 * Created by 丁文豪 on 2018/3/7.
 */
public interface AccountWalletService {
    AccountWallet selectByOpenId(String openId);

    long updateAccountWallet(AccountWallet record);
}
