package com.didispace.service;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/6/20 17:46
 * @company 蜂网供应链管理（上海）有限公司
 */
public interface LockService {

    Boolean lock(String key);
}
