package com.didispace.service;

import com.didispace.entity.AccountWallet;
import com.didispace.mapper.AccountWalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 丁文豪 on 2018/3/7.
 */
@Service
public class AccountWalletServiceImpl implements AccountWalletService{

    @Autowired
    private AccountWalletMapper accountWalletMapper;


    public AccountWallet selectByOpenId(String openId) {
        // TODO Auto-generated method stub
        return accountWalletMapper.selectByOpenId(openId);
    }

    public long updateAccountWallet(AccountWallet record) {
        // TODO Auto-generated method stub
        return accountWalletMapper.updateAccountWallet(record);
    }
}
