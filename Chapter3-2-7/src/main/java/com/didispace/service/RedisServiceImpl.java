//package com.didispace.service;
//
//import com.didispace.entity.RedisModel;
//import org.springframework.stereotype.Service;
//
///**
// * @Description:
// * @Author: MaGuoDong
// * @Date: Created in 2018/6/20 20:13
// * @company 蜂网供应链管理（上海）有限公司
// */
//@Service
//public class RedisServiceImpl extends IRedisService<RedisModel>{
//
//    private static final String REDIS_KEY = "TEST_REDIS_KEY";
//
//    @Override
//    protected String getRedisKey() {
//        return this.REDIS_KEY;
//    }
//}
