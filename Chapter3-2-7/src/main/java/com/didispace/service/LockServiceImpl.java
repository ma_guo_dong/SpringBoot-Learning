package com.didispace.service;

import com.didispace.util.RedisLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: MaGuoDong
 * @Date: Created in 2018/6/20 17:47
 * @company 蜂网供应链管理（上海）有限公司
 */
@Service
public class LockServiceImpl implements LockService{

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Boolean lock(String key) {

        RedisLock redisLock=new RedisLock(redisTemplate,key);
        boolean isFlag= false;
        try {
            isFlag = redisLock.lock();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isFlag;
    }
}
