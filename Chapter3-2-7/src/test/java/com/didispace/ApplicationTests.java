package com.didispace;

import com.didispace.util.HttpResponse;
import com.didispace.util.HttpUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;


public class ApplicationTests {

//	@Autowired
//	private UserMapper userMapper;

	final static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


	//@Test
//	@Rollback
//	public void findByName() throws Exception {
//		userMapper.insert("AAA", 20);
//		User u = userMapper.findByName("AAA");
//		Assert.assertEquals(20, u.getAge().intValue());
//	}

	public static void  main(String [] args){
		CountDownLatch latch=new CountDownLatch(1);//模拟5人并发请求，用户钱包

		for(int i=0;i<5;i++){//模拟5个用户
			Random random=new Random();
			int ran=random.nextInt(2)+1;
			AnalogUser analogUser = new AnalogUser("user"+i,"58899dcd-46b0-4b16-82df-bdfd0d953bfb",ran+"","20.024",latch);
			analogUser.start();
		}
		latch.countDown();//计数器減一  所有线程释放 并发访问。
		System.out.println("所有模拟请求结束  at "+sdf.format(new Date()));
	}

	static class AnalogUser extends Thread{
		String workerName;//模拟用户姓名
		String openId;
		String openType;
		String amount;
		CountDownLatch latch;

		public AnalogUser(String workerName, String openId, String openType, String amount,
						  CountDownLatch latch) {
			super();
			this.workerName = workerName;
			this.openId = openId;
			this.openType = openType;
			this.amount = amount;
			this.latch = latch;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				latch.await(); //一直阻塞当前线程，直到计时器的值为0
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			post();//发送post 请求



		}

		public void post(){
			System.out.println("模拟用户： "+workerName+" 开始发送模拟请求  at "+sdf.format(new Date()));
			String url="http://localhost:8080/wallet/walleroptimisticlock";
			Map map=new HashMap<>();
			map.put("openId",openId);
			map.put("openType",openType);
			map.put("amount",amount);
			HttpResponse response = HttpUtil.post(url,map);
			System.out.println("操作结果："+response.getData());
			System.out.println("模拟用户： "+workerName+" 模拟请求结束  at "+sdf.format(new Date()));

		}





	}

}