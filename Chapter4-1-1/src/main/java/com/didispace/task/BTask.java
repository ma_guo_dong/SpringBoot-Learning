package com.didispace.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executor;

/**
 * Created by 丁文豪 on 2018/3/8.
 */

@Component
public class BTask {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(cron = "0/5 * *  * * ? ")
    public void reportCurrentTime() {
        System.out.println(dateFormat.format(new Date())+"*********B任务每5秒执行一次进入测试");
    }

}
